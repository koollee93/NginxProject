FROM python:3.7-buster

# runtime dependencies
RUN apt-get update && apt-get install -y --no-install-recommends \
		nginx certbot python-certbot-nginx \
	&& apt-get remove --purge --auto-remove -y && rm -rf /var/lib/apt/lists/*


#RUN mkdir -p /var/www/david.techtailors.rs/html
#RUN chmod -R 755 /var/www

COPY ./public/index.html /usr/share/nginx/html
COPY ./sites-available/local.techtailors.rs /etc/nginx/sites-available

RUN ln -s /etc/nginx/sites-available/local.techtailors.rs /etc/nginx/sites-enabled/
RUN rm /etc/nginx/sites-enabled/default

#COPY ./nginx.conf /etc/nginx


EXPOSE 80 443

STOPSIGNAL SIGTERM

CMD ["nginx", "-g", "daemon off;"]


